\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Aufgabenstellung}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Ziel}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Anforderungen}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Teilaufgaben}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Ausgangssituation}{3}{section.2}
\contentsline {section}{\numberline {3}Verbesserung der Emotionserkennung durch die Kamera}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Machine Learning}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Datenbasis}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Kamerabilder}{3}{subsection.3.3}
\contentsline {section}{\numberline {4}Emotionserkennung durch Sprache}{3}{section.4}
\contentsline {section}{\numberline {5}Kombination von Bild und Sprache}{3}{section.5}
\contentsline {section}{\numberline {6}Probleme}{3}{section.6}
\contentsline {section}{\numberline {7}Benutzung}{3}{section.7}
\contentsline {section}{\numberline {8}Ausblick}{3}{section.8}
\contentsline {section}{\numberline {9}Aller Anfang ist schwer}{3}{section.9}
\contentsline {section}{\numberline {10}Die ZIH-Formatvorlage}{3}{section.10}
\contentsline {subsection}{\numberline {10.1}Einbinden der Vorlage}{3}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Verwenden der Vorlage}{4}{subsection.10.2}
\contentsline {subsubsection}{\numberline {10.2.1}Seminararbeiten und ZIH-Artikel}{4}{subsubsection.10.2.1}
\contentsline {subsubsection}{\numberline {10.2.2}Gro{\ss }e Belege und ZIH-Berichte}{6}{subsubsection.10.2.2}
\contentsline {subsubsection}{\numberline {10.2.3}Diplomarbeiten, Bachelor-, Master-Arbeiten}{7}{subsubsection.10.2.3}
\contentsline {subsubsection}{\numberline {10.2.4}Dissertationen}{8}{subsubsection.10.2.4}
\contentsline {subsection}{\numberline {10.3}Zur Verf\"ugung gestellte Befehle}{10}{subsection.10.3}
\contentsline {section}{\numberline {11}Besonderheiten und Hinweise}{11}{section.11}
\contentsline {subsection}{\numberline {11.1}Grafiken und Tabellen}{11}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Literaturverweise}{11}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Dokumentaufteilung}{11}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}Symbol- / Abk\"urzungsverzeichnisse}{11}{subsection.11.4}
\contentsline {subsection}{\numberline {11.5}Umlaute}{12}{subsection.11.5}
\contentsline {subsection}{\numberline {11.6}Kleinigkeiten}{12}{subsection.11.6}
\contentsline {section}{\numberline {12}Anregungen, Fehler und Verbesserungsvorschl\"age}{12}{section.12}
