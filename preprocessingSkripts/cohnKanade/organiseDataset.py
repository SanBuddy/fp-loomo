import glob
from shutil import copyfile

# Organise the Cohn Kanade Dataset, source code from http://www.paulvangent.com/2016/04/01/emotion-recognition-with-python-opencv-and-a-face-dataset/

emotions = ["neutral", "anger", "contempt", "disgust", "fear", "happy", "sadness", "surprise"]
participants = glob.glob("source_emotion\\*") # Returns a list of all lfolders with participant numbers
for x in participants:
    part = "%s" %x[-4] # store the current participant number
    for sessions in glob.glob("%s\\*" %x): # Store list of sessions for the current participant
        for files in glob.glob("%s\\+" %sessions):
            current_session = files[20:-30]
            file = open(files,'r')
            emotion = int(float(file.readline())) # emotions are encoded as float, readline as float, then convert to integer
            sourcefile_emotion = glob.glob("source_images\\%s\\%s\\*" %(part, current_session))[-1] # get path fpr the last image in sequence, which contains the emotion
            sourcefile_neutral = glob.glob("source_images\\%s\\%s\\*" %(part, current_session))[0] # do the same for the neutral image
            dest_neut = "sorted_set\\neutral\\%s" %sourcefile_neutral[25:] # Generate the path to put neutral image
            dest_emot = "sorted_set\\%s\\%s" %(emotions[emotion], sourcefile_emotion[25:]) # Do the same for emotion containing image
            copyfile(sourcefile_neutral, dest_neut)
            copyfile(sourcefile_emotion, dest_emot)