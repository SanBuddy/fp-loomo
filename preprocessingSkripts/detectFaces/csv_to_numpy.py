from constants import CASC_PATH, SIZE_FACE, EMOTIONS, SAVE_DATASET_IMAGES_FILENAME, SAVE_DATASET_LABELS_FILENAME, SAVE_DIRECTORY, DATASET_CSV_FILENAME
import cv2
import pandas as pd
import numpy as np
from PIL import Image
from os.path import join
import os
import scipy.misc
import csv

cascade_classifier = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eyeCascade  = cv2.CascadeClassifier("haarcascade_eye.xml")



def format_image(image):
    if len(image.shape) > 2 and image.shape[2] == 3:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
        image = cv2.imdecode(image, cv2.IMREAD_GRAYSCALE)
    image = cv2.resize(image, (224, 224))  # resize, so all pictures have same size

    # normalisierung scheint nicht mehr nötig, da es zum Histogrammausgleich kommt
    #image = cv2.normalize(src=image, dst=image, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX,
    #                                dtype=cv2.CV_8U)
    cv2.equalizeHist(image, image)
    # TODO: hier mal mit den Parametern spielen, eventeull lässt sich da noch was verbessern
    faces = cascade_classifier.detectMultiScale(
        image,
        scaleFactor=1.1,
        minNeighbors=5
    )
    #  None is we don't found an image
    if not len(faces) > 0:
        print("no face detected")
        return None
    max_area_face = faces[0]
    for face in faces:
        if face[2] * face[3] > max_area_face[2] * max_area_face[3]:
            max_area_face = face
    # Chop image to face
    face = max_area_face
    image = image[face[1]:(face[1] + face[2]), face[0]:(face[0] + face[3])]
    # Resize image to network size
    try:
        image = cv2.resize(image, (224, 224))  # resize, so all pictures have same size
        eyes = eyeCascade.detectMultiScale(image)
        eyesCoordinates = [None] * 2
        eyeCount = 0
        # avoid false positives
        for (ex, ey, ew, eh) in eyes:
            if len(eyes) < 2:   # if there aren't 2 eyes found, something seems to be off with the picture
                return  None
            if eyeCount == 2:
                # only rotate when 2 eyes were found
                x = eyesCoordinates[0][1] - eyesCoordinates[1][1]
                y = eyesCoordinates[0][0] - eyesCoordinates[1][0]
                angle = cv2.fastAtan2(x, y) - 180
                print("rotating by ", angle)
                num_rows, num_cols = image.shape[:2]
                rotation_matrix = cv2.getRotationMatrix2D((num_cols / 2, num_rows / 2), angle, 1)
                image = cv2.warpAffine(image, rotation_matrix, (num_cols, num_rows))
                break
            if 25 <= ey <= 80:
                if ex <= 80:
                    eyesCoordinates[0] = (ex, ey)
                    eyeCount += 1
                elif ex >= 130:
                    eyesCoordinates[1] = (ex, ey)
                    eyeCount += 1
    except Exception:
        print("[+] Problem during resize")
        return None
    return image


def emotion_to_vec(x):
    d = np.zeros(len(EMOTIONS))
    d[x] = 1.0
    return d


def flip_image(image):
    return cv2.flip(image, 1)


def data_to_image(data):
    data_image = np.fromstring(
        str(data), dtype=np.uint8, sep=' ').reshape((SIZE_FACE, SIZE_FACE))
    data_image = Image.fromarray(data_image).convert('RGB')
    data_image = np.array(data_image)[:, :, ::-1].copy()
    data_image = format_image(data_image)
    return data_image


data = pd.read_csv('fer2013.csv')





labels = []
images = []
index = 1
total = data.shape[0]
for index, row in data.iterrows():
    emotion = emotion_to_vec(row['emotion'])
    image = data_to_image(row['pixels'])
    if image is not None:

        labels.append(emotion)
        images.append(image)
        stacked_image = np.dstack((image,) * 3)
        image_folder = os.path.join(EMOTIONS[row[0]])
        if not os.path.exists(image_folder):
            os.mkdir(image_folder)
        image_file = os.path.join(image_folder, str(index) + '.jpg')
        scipy.misc.imsave(image_file, stacked_image)
    index += 1
    print("Progress: {}/{} {:.2f}%".format(index, total, index * 100.0 / total))

print("Total: " + str(len(images)))
np.save(join(SAVE_DATASET_IMAGES_FILENAME), images)
np.save(join(SAVE_DATASET_LABELS_FILENAME), labels)