# use for ck+

import cv2
import glob
import numpy as np


faceDet = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eyeCascade  = cv2.CascadeClassifier("haarcascade_eye.xml")
emotions = ["neutral", "anger", "contempt", "disgust", "fear", "happy", "sadness", "surprise"] #Define emotions
def detect_faces(emotion):
    files = glob.glob("sorted_set/%s/*" %emotion) # get a list of all images with their emotions
    filenumber = 0
    for f in files:
        frame = cv2.imread(f) # Open image
        frame = cv2.resize(frame, (350, 350))
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        normalizedImage = np.zeros((350, 350)) # init the normalized face
        normalizedImage = cv2.normalize(src=gray, dst=normalizedImage, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        face = faceDet.detectMultiScale(normalizedImage, scaleFactor=1.1, minNeighbors=10, minSize=(5,5),
                                        flags= cv2.CASCADE_SCALE_IMAGE)
        if len(face) == 1: # only possible to detect either one or no face
            facefeatures = face
            for (x, y, w, h) in face:
                cv2.rectangle(normalizedImage, (x,y), (x+w, y+h), (255,0,0),2)
                roi_gray = normalizedImage[y:y+h, x:x+w]
                roi_color = normalizedImage[y:y+h, x:x+w]
                eyes = eyeCascade.detectMultiScale(roi_gray)
                eyesCoordinates = [None] * 2

                # avoid false positives: (rough position is none, as all the pictures are setup and the resolution is always similar)
                for (ex, ey, ew, eh) in eyes: # to check the eyepositions, it's enough to just check these values
                    if 25 <= ey <= 80: # y coordinate stays the same:
                        if ex <= 80:
                            eyesCoordinates[0] = (ex, ey)
                            #cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
                        elif 80 < ex:
                            eyesCoordinates[1] = (ex, ey)
                            #cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

                if eyesCoordinates[0] is not None and eyesCoordinates[1] is not None:
                    x = eyesCoordinates[0][1] - eyesCoordinates[1][1]
                    y = eyesCoordinates[0][0] - eyesCoordinates[1][0]
                    angle = cv2.fastAtan2(x, y) - 180 # -180 to avoid the picture being flipped
                else:
                    angle = 0
                #if  -45 < angle < 45: # indicates, that there still is a false positive included
                #    angle = 0
                print("angle", angle)

        else:
            facefeatures = ""
        # cut and save face
        for (x, y, w, h) in facefeatures:

            # get coordinates and size of rectangle containing face
            #print("face found in file %s" %f)
            normalizedImage = normalizedImage[y:y+h, x:x+w] # cut the frame to size
            num_rows, num_cols = normalizedImage.shape[:2]
            if angle:
                print("rotating by", angle, f)
                rotation_matrix = cv2.getRotationMatrix2D((num_cols/2, num_rows/2), angle, 1)
                normalizedImage = cv2.warpAffine(normalizedImage, rotation_matrix, (num_cols, num_rows))
            try:
                out = cv2.resize(normalizedImage, (350, 350)) # resize, so all pictures have same size
                cv2.imwrite("dataset/%s/%s.jpg" %(emotion, filenumber), out) # write image
            except:
                pass # pass file in case of error
        filenumber += 1

for emotion in emotions:
    detect_faces(emotion)