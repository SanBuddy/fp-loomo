package com.example.android.camera2video;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class TensorFlow {

    // name of the model/label file stored in Assets
    private static final String MODEL_PATH = "graph.lite";
    private static final String LABEL_PATH = "labels.txt";
    private static final int imageDimensions = 224;
    private static final int DIM_BATCH_SIZE = 1;
    private static final int DIM_PIXEL_SIZE = 3;

    // ByteBuffer to keep the image data to be fed into TF Lite as inputs
    private ByteBuffer imgData;

    private List<String> labelList;
    private float[][] labelProbArray = null;
    private static final int IMAGE_MEAN = 128;
    private static final float IMAGE_STD = 128.0f;

    /* Preallocated buffers for storing image data in. */
    private int[] intValues = new int[imageDimensions * imageDimensions];
    private Interpreter tflite;
    private static final int RESULTS_TO_SHOW = 7;

    private PriorityQueue<Map.Entry<Integer, Float>> sortedLabels =
            new PriorityQueue<>(
                    RESULTS_TO_SHOW,
                    new Comparator<Map.Entry<Integer, Float>>() {
                        @Override
                        public int compare(Map.Entry<Integer, Float> o1, Map.Entry<Integer, Float> o2) {
                            return (o1.getValue()).compareTo(o2.getValue());
                        }
                    });

    TensorFlow(Activity activity) throws IOException {
        tflite = new Interpreter(loadModelFile(activity));
        labelList = loadLabelList(activity);
        imgData =
                ByteBuffer.allocateDirect(
                        4 * DIM_BATCH_SIZE * imageDimensions * imageDimensions * DIM_PIXEL_SIZE);
        imgData.order(ByteOrder.nativeOrder());
        labelProbArray = new float[1][labelList.size()];
        Log.d("classfier", "created TF Classifier");

    }



    float[] predictEmotion(Bitmap bitmap) {

        // scale the input bitmap to the size of the pictures for the mobileNet
        bitmap = Bitmap.createScaledBitmap(bitmap, imageDimensions, imageDimensions, true);
        convertBitmapToByteBuffer(bitmap);
        // here's where the magic happes
        Mat mat = new Mat();
        Utils.bitmapToMat(bitmap, mat);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(mat, mat);
        Utils.matToBitmap(mat, bitmap);
        tflite.run(imgData, labelProbArray);
        float[] result = labelProbArray[0];

        if (result != null) {
            return result;
        } else {
            return null;
        }
    }

    /**
     * Reads label list from Assets.
     */
    private List<String> loadLabelList(Activity activity) throws IOException {
        List<String> labelList = new ArrayList<String>();
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(activity.getAssets().open(LABEL_PATH)));
        String line;
        while ((line = reader.readLine()) != null) {
            labelList.add(line);
        }
        reader.close();
        return labelList;
    }

    /**
     * Memory-map the model file in Assets.
     */
    private MappedByteBuffer loadModelFile(Activity activity) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_PATH);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    /**
     * Writes Image data into a {@code ByteBuffer}.
     */
    private void convertBitmapToByteBuffer(Bitmap bitmap) {
        if (imgData == null) {
            return;
        }
        imgData.rewind();
        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        // Convert the image to floating point.
        int pixel = 0;
        for (int i = 0; i < imageDimensions; ++i) {
            for (int j = 0; j < imageDimensions; ++j) {
                final int val = intValues[pixel++];
                imgData.putFloat((((val >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat((((val >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat((((val) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
            }
        }
    }


    private void applyWeight(List<Integer> emotion){
        // android studio says that function is not used, but it is used in emojiFace

       for (int i = 0; i < labelList.size(); i++){


            if (emotion.contains(i)){
                labelProbArray[0][i] = labelProbArray[0][i] * 2;
            }
            else
                labelProbArray[0][i] = (float) (labelProbArray[0][i] * 0.5);
            System.out.println("after value  = " + labelProbArray[0][i]);
            System.out.println(" ");
        }

    }

}