package com.example.android.camera2video;

public class Sad implements Emotion {
    //Image emoji;
    int emoji = R.drawable.sad;

    double emojiRatio = 1.09;

    @Override
    public int getEmoji() {
        return emoji;
    }

    public double getEmojiRatio() {
        return emojiRatio;
    }
}
