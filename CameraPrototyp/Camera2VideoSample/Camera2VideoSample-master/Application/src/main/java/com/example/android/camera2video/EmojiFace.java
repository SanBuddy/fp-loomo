package com.example.android.camera2video;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.camera2.params.Face;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmojiFace {
    private Emotion emotion = new Thinking();
    private Integer confidence = null;
    private Bitmap faceImage;
    private boolean marked;
    private ImageProcessor imgProcessor = new ImageProcessor();
    private Face face;
    private RectF faceRect;
    private TensorFlow tensorFlow;
    private PictureEmotion pictureEmotion;
    private Speech speech;
    public Integer atmosphere;


    EmojiFace(Face face) {
        this.face = face;
        this.faceRect = transformRect(calculateFaceRange(face.getBounds()));
        this.marked = true;
    }


    public void setTensorFlow(TensorFlow tensorFlow) {
        this.tensorFlow = tensorFlow;
    }

    public void setPictureEmotion(PictureEmotion pictureEmotion) {
        this.pictureEmotion = pictureEmotion;
    }

    public void setSpeech(Speech speech) {
        this.speech = speech;
    }

    /**
     * updates the faceImage from the current frame
     *
     * @param frame
     */
    public void updateImage(Bitmap frame) {
        /**
         * Rotation und Skalierung
         */
        Bitmap transformedBitmap = imgProcessor.transform(frame);

        faceImage = imgProcessor.cropImage(transformedBitmap, faceRect);

    }

    /**
     * Emotion Recognition
     * sets emotion and confidence of this face
     */
    public void predictEmotion() {
        //get the emotion from speech
        SpeechEmotion speechResult = speech.emotionFromSpeech();
        SpeechEmotion oldSpeechResult = speech.oldEmotionFromSpeech();


        int currentEmotion = 0;
        float currentConfidence = 0;
        if (faceImage != null) {
            pictureEmotion.setTimestamp(System.currentTimeMillis());
            float[] emotionResult = tensorFlow.predictEmotion(faceImage);
            System.out.println("emotionresult " + Arrays.toString(emotionResult));

            for (int i = 0; i < emotionResult.length; i++) {
                float confidence = emotionResult[i];
                if (pictureEmotion.confidenceList.get((i)).size() < 3) {
                    pictureEmotion.confidenceList.get(i).add(confidence);
                } else {
                    pictureEmotion.confidenceList.get(i).remove(0);
                    pictureEmotion.confidenceList.get(i).add(confidence);
                }
            }

            for (int i = 0; i < pictureEmotion.confidenceList.size(); i++) {
                pictureEmotion.pictureAtmosphere.set(i, 0);
                float sum = 0;
                for (int j = 0; j < pictureEmotion.confidenceList.get(i).size(); j++) {
                    sum += pictureEmotion.confidenceList.get(i).get(j);
                }
                float average = sum / pictureEmotion.confidenceList.get(i).size();
                pictureEmotion.averageConfidence.set(i, average);
            }

        }

        long newDiff = Math.abs(pictureEmotion.getTimestamp() - speechResult.getTimestamp());
        long oldDiff = Math.abs(pictureEmotion.getTimestamp() - oldSpeechResult.getTimestamp());
        int aroused = -10;
        int positivity = -10;

        // if the closest time is older than 3 seconds, don't use it
        if(oldDiff < newDiff){
            if (oldDiff < 3000) {
                aroused = oldSpeechResult.getArousal();
                positivity = oldSpeechResult.getValence();
            }
        } else {
            if (newDiff < 3000) {
                aroused = speechResult.getArousal();
                positivity = speechResult.getValence();
            }
        }


        List<Integer> emotionLower = new ArrayList<>();
        switch (aroused){
            case -1:
                for (int i = 0; i < pictureEmotion.averageConfidence.size(); i++) {
                    if (i != 5){
                        emotionLower.add(i);
                    }
                }
                applyWeight(emotionLower);
                break;
            case 1:
                emotionLower.add(4);
                emotionLower.add(5);
                applyWeight(emotionLower);
                break;
            default:
                break;
        }

        // no positivity values implemented yet
        switch (positivity){
            case -1:
                break;
            case 1:
                break;
            default:
                break;
        }

        float average;


        for (int i = 0; i < pictureEmotion.averageConfidence.size(); i++){
            average = pictureEmotion.averageConfidence.get(i);
            if (average > currentConfidence) {
                currentEmotion = i;
                currentConfidence = average;
            }

        }
        ArrayList<Integer> lastEmotion = new ArrayList<>();
        for (int i = 0; i < pictureEmotion.averageConfidence.size(); i++){
            if (i == currentEmotion){
                lastEmotion.add(1);
            }
            else {
                lastEmotion.add(0);
            }
        }

        if (pictureEmotion.lastEmotions.size() < 10){
            pictureEmotion.lastEmotions.add(lastEmotion);
        }
        else {
            pictureEmotion.lastEmotions.remove(0);
            pictureEmotion.lastEmotions.add(lastEmotion);

        }

        for (int i = 0; i < pictureEmotion.lastEmotions.size(); i++){
            for (int j = 0; j < pictureEmotion.lastEmotions.get(i).size(); j++){
                if (pictureEmotion.lastEmotions.get(i).get(j) == 1){
                    int atmosphereEmotion = pictureEmotion.pictureAtmosphere.get(j);
                    atmosphereEmotion += 1;
                    pictureEmotion.pictureAtmosphere.set(j, atmosphereEmotion);

                }

            }
        }
        int emotionCount = 0;

        for (int i = 0; i < pictureEmotion.pictureAtmosphere.size(); i++){
            if (pictureEmotion.pictureAtmosphere.get(i) > emotionCount){
                emotionCount = pictureEmotion.pictureAtmosphere.get(i);
                atmosphere = i;
            }
        }
        switch (atmosphere){
            case 0:
                pictureEmotion.atmosphereToBeSet = "angry";
                break;
            case 1:
                pictureEmotion.atmosphereToBeSet = "disgusted";
                break;
            case 2:
                pictureEmotion.atmosphereToBeSet = "scared";
                break;
            case 3:
                pictureEmotion.atmosphereToBeSet = "happy";
                break;
            case 4:
                pictureEmotion.atmosphereToBeSet = "neutral";
                break;
            case 5:
                pictureEmotion.atmosphereToBeSet = "sad";
                break;
            case 6:
                pictureEmotion.atmosphereToBeSet = "surprised";
                break;
            default:
                pictureEmotion.atmosphereToBeSet = "neutral";
                break;
        }
        
        currentConfidence = pictureEmotion.averageConfidence.get(currentEmotion);

        switch (currentEmotion) {
            case 0:
                setEmotion(new Angry());
                break;
            case 1:
                setEmotion(new Disgusted());
                break;
            case 2:
                setEmotion(new Scared());
                break;
            case 3:
                setEmotion(new Happy());

                break;
            case 4:
                setEmotion(new Neutral());

                break;
            case 5:
                setEmotion(new Sad());
                break;
            case 6:
                setEmotion(new Surprised());

                break;
            default:
                setEmotion(new Neutral());


        }

        int f_neu = (Math.round(currentConfidence * 100));

        setConfidence(f_neu);
    }

    private void applyWeight(List<Integer> emotionLower){

        for (int i = 0; i < pictureEmotion.averageConfidence.size(); i ++){
            float confidence = pictureEmotion.averageConfidence.get(i);
            if (emotionLower.contains(i)){
                confidence = (float)(confidence * 0.5);
            }
            else {
                confidence = (float)(confidence * 2);
            }
            pictureEmotion.averageConfidence.set(i, confidence);
        }
    }

    public RectF calculateFaceRange(final Rect face) {
        int viewWidth = 640;
        int viewHeight = 480;
        RectF rectF = FaceUtil.calculateFaceRect(face, viewWidth, viewHeight, 3264,
                2448);
        return rectF;
    }

    private RectF transformRect(RectF rectF) {
        int viewWidth = 640;
        int viewHeight = 480;
        return FaceUtil.transformRect(rectF, viewWidth, viewHeight);
    }

    public void updateFace(Face face) {
        this.face = face;
        this.faceRect = transformRect(calculateFaceRange(face.getBounds()));
    }

    public boolean isMarked() {
        return marked;
    }

    public int emojiXPosition() {
        return (int) faceRect.left;
    }

    public int emojiYPosition() {
        return (int) faceRect.top;
    }

    public int emojiWidth() {
        return (int) faceRect.width();
    }

    public int emojiHeight() {
        return (int) faceRect.height();
    }

    public Emotion getEmotion() {
        return emotion;
    }

    public void setEmotion(Emotion emotion) {
        this.emotion = emotion;
    }

    public Bitmap getFaceImage() {
        return faceImage;
    }

    public Face getFace() {
        return face;
    }

    public void setFace(Face face) {
        this.face = face;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }
}
