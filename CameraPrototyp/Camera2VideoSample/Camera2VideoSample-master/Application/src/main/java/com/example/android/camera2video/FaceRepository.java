package com.example.android.camera2video;

import android.graphics.Bitmap;
import android.hardware.camera2.params.Face;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FaceRepository {
    private ArrayList<EmojiFace> emojiFaces = new ArrayList<>();
    private Bitmap frame;
    private TensorFlow tf;
    private PictureEmotion pictureEmotion;
    private int countFrames = 0;
    private int countFramesLimit = 30;
    private Date startTime;
    private Date endTime;
    private Speech speech;

    public void insert(Face[] faces, Bitmap frame) {
        this.frame = frame;
        /**
         * Option 1: delete List and add new faces
         */
        /*emojiFaces.clear();
        for (Face face : faces) {
            add(face);
        }*/


        /**
         * Option 2: Face Recognition
         */
        for (Face face : faces) {
            EmojiFace ef = hasEmojiFace(face);
            if (ef == null) {
                //add new emojiFace
                add(face);
            } else {
                //face exists
                update(face, ef);
            }
        }

        deleteNonMarkedEmojiFaces();

        /**
         * Just with Option 1
         * lastEmotion = emojiFaces.get(0).getEmotion();
         */
        if (countFrames == 0){
            startTime = Calendar.getInstance().getTime();
        }
        countFrames++;
        if (countFrames == countFramesLimit) {
            endTime = Calendar.getInstance().getTime();
            long timedifference = endTime.getTime() - startTime.getTime();
            System.out.println("time needed for " + countFramesLimit + " frames: " + timedifference +" ms");
            countFrames = 0;
        }

    }

    /**
     * return EmojiFace if Face with FaceId exists, else return null
     *
     * @param face
     * @return EmojiFace|null
     */
    public EmojiFace hasEmojiFace(Face face) {
        for (EmojiFace emojiFace : emojiFaces) {
            if (face.getId() == emojiFace.getFace().getId()) {
                return emojiFace;
            }
        }

        return null;
    }

    /**
     * Updates emojiFace with new Face data and new camera frame
     *
     * @param face
     * @param emojiFace
     */
    public void update(Face face, EmojiFace emojiFace) {
        emojiFace.updateFace(face);
        if (countFrames == 0) {
            emojiFace.setPictureEmotion(pictureEmotion);
            emojiFace.updateImage(frame);
            emojiFace.setSpeech(speech);
            emojiFace.setTensorFlow(tf);
            emojiFace.predictEmotion();
        }
        emojiFace.setMarked(true);
    }

    /**
     * Creates new EmojiFace with face data and camera frame and adds it to emojiFace List
     *
     * @param face
     */
    public void add(Face face) {
        EmojiFace ef = new EmojiFace(face);
        if (countFrames == 0) {
            ef.setPictureEmotion(pictureEmotion);
            ef.updateImage(frame);
            ef.setSpeech(speech);
            ef.setTensorFlow(tf);
            ef.predictEmotion();
        } /*else {
            //Just for Option 1
            //ef.setEmotion(lastEmotion);
        }*/
        emojiFaces.add(ef);

    }

    /**
     * deletes non Marked (not in the frame existing) emojiFaces
     */
    public void deleteNonMarkedEmojiFaces() {
        ArrayList<EmojiFace> removeList = new ArrayList<>();
        for (EmojiFace emojiFace : emojiFaces) {
            if (!emojiFace.isMarked()) {
                //if emojiFace is not marked -> delete
                removeList.add(emojiFace);
            } else {
                //if emojiFace is marked -> reset for next round: unmark
                emojiFace.setMarked(false);
            }
        }
        emojiFaces.removeAll(removeList);
    }

    public ArrayList<EmojiFace> getEmojiFaces() {
        return emojiFaces;
    }

    public void setTf(TensorFlow tf) {
        this.tf = tf;
    }

    public void setPictureEmotion(PictureEmotion pictureEmotion) {
        this.pictureEmotion = pictureEmotion;
    }

    public void setSpeech(Speech speech){
        this.speech = speech;
    }


}
