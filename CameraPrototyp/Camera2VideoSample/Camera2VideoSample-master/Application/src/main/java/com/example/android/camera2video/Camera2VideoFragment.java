/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.camera2video;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.Face;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.segway.robot.algo.dts.DTSPerson;
import com.segway.robot.algo.dts.PersonTrackingListener;
import com.segway.robot.sdk.base.bind.ServiceBinder;
import com.segway.robot.sdk.base.log.Logger;
import com.segway.robot.sdk.locomotion.head.Head;
import com.segway.robot.sdk.vision.DTS;
import com.segway.robot.sdk.vision.Vision;
import com.segway.robot.support.control.HeadPIDController;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.SilenceDetector;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;

import static org.opencv.core.CvType.CV_8U;

/**
 * @author jacob
 * @author Felix Lipinski
 * @author Martin Eisoldt
 * @author Thomas Hauptvogel
 */
public class Camera2VideoFragment extends Fragment implements View.OnClickListener {


    private static final String TAG = "Camera2VideoFragment";

    private static final int PREVIEW_WIDTH = 640;
    private static final int PREVIEW_HEIGHT = 480;

    private FaceRepository faceRepository = new FaceRepository();
    public TensorFlow tensorFlow;
    public Speech speech;
    public PictureEmotion pictureEmotion;
    /**
     * A reference to the opened {@link android.hardware.camera2.CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * A reference to the current {@link android.hardware.camera2.CameraCaptureSession} for
     * preview.
     */
    private CameraCaptureSession mPreviewSession;

    private CaptureRequest.Builder mPreviewBuilder;
    private AutoFitDrawableView autoFitDrawableView;

    private Vision mVision;
    private Head mHead;
    private boolean isVisionBind;
    private boolean isHeadBind;
    private boolean isSurfaceTextureAvailable;
    private DTS dts;
    private Surface mDTSSurface = null;
    private HeadPIDController headPIDController = new HeadPIDController();

    // Audio Related Variables

    private Thread audioThread;
    private AudioDispatcher dispatcher;
    private AudioProcessor pitchProcessor;
    private SoundDetector soundDetector;

    private List pitches, volumes, time;
    private float arousal;
    private float minPitch, maxPitch, avgPitch;
    private double minVol, maxVol, avgVol;
    private float newPitch, oldPitch;
    private double newVolume, oldVolume;
    private long newTime, oldTime;
    private long startTime, endTime;
    SilenceDetector silenceDetector;
    Iterator<Long> timeIterator;


    private void bindServices() {
        mVision = Vision.getInstance();
        mHead = Head.getInstance();

        mVision.bindService(getActivity(), visionBindStateListener);
        mHead.bindService(getActivity(), headBindStateListener);

    }

    private ServiceBinder.BindStateListener visionBindStateListener = new ServiceBinder.BindStateListener() {
        @Override
        public void onBind() {
            Logger.e(TAG, "Vision onBind() called");
            isVisionBind = true;
            dts = mVision.getDTS();
            dts.setVideoSource(DTS.VideoSource.SURFACE);
            dts.start();
            mDTSSurface = dts.getSurface();
            dts.setPoseRecognitionEnabled(true);

            if (isSurfaceTextureAvailable) {
                openCamera(autoFitDrawableView.getWidth(), autoFitDrawableView.getHeight());
            }
        }

        @Override
        public void onUnbind(String reason) {
            isVisionBind = false;
        }
    };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Creating the Detector and Handler for Pitch and Volume
        pitches = new ArrayList();
        volumes = new ArrayList();
        time = new ArrayList();
        timeIterator = this.time.iterator();

        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(22050, 1024, 0);
        soundDetector = new SoundDetector();
        silenceDetector = soundDetector.getSilenceDetector();
        PitchDetectionHandler pdh = new PitchDetectionHandler() {
            @Override
            public void handlePitch(PitchDetectionResult res, AudioEvent e) {
                final float pitchInHz = Math.round(res.getPitch() * 100f) / 100f;
                final double volumeInDb = Math.round(soundDetector.getCurrentVolume() * 100d) / 100d;
                if(((System.currentTimeMillis() - startTime) / 1000) > 30){
                    speech.setSoundParameters(0, 0, System.currentTimeMillis());
                    startTime = System.currentTimeMillis();
                }
                if (pitchInHz > -1) {
                    pitches.add(pitchInHz);
                    volumes.add(volumeInDb);
                    if (pitches.size() == 20 || volumes.size() == 20) {
                        processAudioData();
                    }
                }
            }
        };
        // Creating and starting the audio thread
        pitchProcessor = new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN, 22050, 1024, pdh);
        dispatcher.addAudioProcessor(pitchProcessor);
        dispatcher.addAudioProcessor(silenceDetector);
        dispatcher.addAudioProcessor(soundDetector);
        audioThread = new Thread(dispatcher, "Audio Thread");
        audioThread.start();
        pictureEmotion = new PictureEmotion();
        speech = new Speech();
        try {
            tensorFlow = new TensorFlow(getActivity());

            System.out.println("created classifier");
        } catch (IOException e) {
            Log.e("error", "Failed to initialize an image classifier");
        }
        faceRepository.setPictureEmotion(pictureEmotion);
        faceRepository.setSpeech(speech);
        faceRepository.setTf(tensorFlow);
    }

    /**
     * Initializes all relevant audio variables.
     */
    @Override
    public void onStart() {
        super.onStart();
        startTime = System.currentTimeMillis();
        minPitch = 9999;
        maxPitch = 0;
        avgPitch = 0;
        minVol = 9999;
        maxVol = 0;
        avgVol = 0;
        newPitch = 0;
        newVolume = 0;
        newTime = 0;
        arousal = 0;
    }

    private ServiceBinder.BindStateListener headBindStateListener = new ServiceBinder.BindStateListener() {
        @Override
        public void onBind() {
            Logger.e(TAG, "Head onBind() called");
            isHeadBind = true;
            mHead.setMode(Head.MODE_ORIENTATION_LOCK);
            mHead.setWorldPitch(0.3f);
            headPIDController.init(new HeadControlHandlerImpl(mHead));
            headPIDController.setHeadFollowFactor(1.0f);
        }


        @Override
        public void onUnbind(String reason) {
            isHeadBind = false;
        }
    };

    private void startTrackingPerson() {
        dts.startPersonTracking(null, 15L * 60 * 1000 * 1000, new PersonTrackingListener() {
            @Override
            public void onPersonTracking(DTSPerson person) {
                if (person == null) {
                    return;
                }

                if (isHeadBind) {
                    headPIDController.updateTarget(person.getTheta(), person.getDrawingRect(), 480);
                }
            }

            @Override
            public void onPersonTrackingResult(DTSPerson person) {
                Logger.d(TAG, "onPersonTrackingResult() called with: person = [" + person + "]");
            }

            @Override
            public void onPersonTrackingError(int errorCode, String message) {
                Logger.e(TAG, "onPersonTrackingError() called with: errorCode = [" + errorCode + "], message = [" + message + "]");
            }
        });
    }


    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            isSurfaceTextureAvailable = true;
            if (isSurfaceTextureAvailable && isVisionBind) {
                openCamera(width, height);
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            isSurfaceTextureAvailable = false;
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

    };

    /**
     * The {@link android.util.Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * The {@link android.util.Size} of video recording.
     */
    private Size mVideoSize;

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * A {@link Semaphore} to prevent the app from eonactxiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its status.
     */
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            startPreview();
            mCameraOpenCloseLock.release();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                activity.finish();
            }
        }

    };

    public static Camera2VideoFragment newInstance() {
        return new Camera2VideoFragment();
    }

    /**
     * In this sample, we choose a video size with 3x4 aspect ratio. Also, we don't use sizes
     * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
     *
     * @param choices The list of available sizes
     * @return The video size
     */
    private static Size chooseVideoSize(Size[] choices) {
        for (Size size : choices) {
            if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
                return size;
            }
        }
        Logger.e(TAG, "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Logger.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera2_video, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        autoFitDrawableView = view.findViewById(R.id.texture);
        /**
         * Single Tensorflow initializiation for Emotion Recognition
         * handover tensorflow one time to faceRepository
         */
        bindServices();
    }

    @Override
    public void onResume() {
        super.onResume();
        startBackgroundThread();
        if (autoFitDrawableView.getPreview().isAvailable()) {
            openCamera(autoFitDrawableView.getPreview().getWidth(), autoFitDrawableView.getPreview().getHeight());
        } else {
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            autoFitDrawableView.setPreviewSizeAndRotation(PREVIEW_WIDTH, PREVIEW_HEIGHT, rotation);
            autoFitDrawableView.getPreview().setSurfaceTextureListener(mSurfaceTextureListener);
        }
        if(dispatcher.isStopped()){
            dispatcher.run();
        }
    }

    @Override
    public void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
        dispatcher.stop();
    }

    @Override
    public void onClick(View view) {
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tries to open a {@link CameraDevice}. The result is listened by `mStateCallback`.
     */
    @SuppressWarnings("MissingPermission")
    private void openCamera(int width, int height) {
        final Activity activity = getActivity();
        if (null == activity || activity.isFinishing()) {
            return;
        }
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            Logger.e(TAG, "tryAcquire");
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            String cameraId = manager.getCameraIdList()[0];

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map == null) {
                throw new RuntimeException("Cannot get available preview/video sizes");
            }
            mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), width, height, mVideoSize);
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            autoFitDrawableView.setPreviewSizeAndRotation(mPreviewSize.getWidth(), mPreviewSize.getHeight(), rotation);

            manager.openCamera(cameraId, mStateCallback, null);
        } catch (CameraAccessException e) {
            Toast.makeText(activity, "Cannot access the camera.", Toast.LENGTH_SHORT).show();
            activity.finish();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.");
        }
    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            closePreviewSession();
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.");
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Start the camera preview.
     */
    private void startPreview() {
        if (null == mCameraDevice || !autoFitDrawableView.getPreview().isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            SurfaceTexture texture = autoFitDrawableView.getPreview().getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            List<Surface> surfaceList = new ArrayList<>();

            Surface previewSurface = new Surface(texture);

            surfaceList.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);
            Logger.e(TAG, "startPreview() called " + mDTSSurface.isValid());
            surfaceList.add(mDTSSurface);
            mPreviewBuilder.addTarget(mDTSSurface);


            mCameraDevice.createCaptureSession(surfaceList,
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                            startTrackingPerson();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Activity activity = getActivity();
                            if (null != activity) {
                                Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the camera preview. {@link #startPreview()} needs to be called in advance.
     */
    private void updatePreview() {
        if (null == mCameraDevice) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), mFaceCaptureCallBack, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Here the magic happens
     * Face Recognition from CameraStream
     * Faces to FaceRepository, there face and emotion processing
     * drawing of results in autoFitDrawableView
     */

    private CameraCaptureSession.CaptureCallback mFaceCaptureCallBack = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            Face[] faces = result.get(CaptureResult.STATISTICS_FACES);// detected face array
            if (faces != null && faces.length > 0) {
                Bitmap frame = autoFitDrawableView.getPreview().getBitmap();
                Mat mat = new Mat();
                Utils.bitmapToMat(frame, mat);
                mat.convertTo(mat, CV_8U);
                Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
                Imgproc.equalizeHist(mat, mat);
                Utils.matToBitmap(mat, frame);
                faceRepository.insert(faces, frame);
                autoFitDrawableView.drawRect(faceRepository, getCameraFragment());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView atmosphereText = autoFitDrawableView.findViewById(R.id.atmosphereText);
                        atmosphereText.setText("Atmosphere: "+ pictureEmotion.atmosphereToBeSet);
                    }
                });
            }
            else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView atmosphereText = autoFitDrawableView.findViewById(R.id.atmosphereText);
                        atmosphereText.setText("Atmosphere: ");
                    }

                });
            }

        }

    };

    /**
     * setup Camera and FaceDetection Mode
     *
     * @param builder
     */
    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        builder.set(CaptureRequest.STATISTICS_FACE_DETECT_MODE, CameraMetadata.STATISTICS_FACE_DETECT_MODE_FULL);
    }

    private void closePreviewSession() {
        if (mPreviewSession != null) {
            mPreviewSession.close();
            mPreviewSession = null;
        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    /**
     * Processes the minimum, maximum and average volume and pitch,
     * and the time needed to record the samples. Then clears the
     * audio samples.
     */
    private void processAudioData() {
        Iterator<Float> pitchIterator = this.pitches.iterator();
        Iterator<Double> volumeIterator = this.volumes.iterator();
        float pitch;
        double volume;
        float medPitch = 0;
        double medVol = 0;
        minPitch = 999;
        maxPitch = 0;
        minVol = 999;
        maxVol = 0;

        // Process the minimum, maximum and average pitch
        while (pitchIterator.hasNext()) {
            pitch = pitchIterator.next();
            if (pitch < minPitch) {
                minPitch = pitch;
            }
            if (pitch > maxPitch) {
                maxPitch = pitch;
            }
            medPitch += pitch;
        }

        oldPitch = avgPitch;
        avgPitch = Math.round((medPitch / this.pitches.size()) * 100f) / 100f;
        newPitch = avgPitch;

        // Process the minimum, maximum and average volume
        while (volumeIterator.hasNext()) {
            volume = volumeIterator.next();
            if (volume < minVol) {
                minVol = volume;
            }
            if (volume > maxVol) {
                maxVol = volume;
            }
            medVol += volume;
        }

        oldVolume = avgVol;
        avgVol = Math.round((medVol / this.volumes.size()) * 100d) / 100d;
        newVolume = avgVol;

        // Delete the old values
        this.volumes.clear();
        this.pitches.clear();

        // Save time needed to record the samples
        endTime = (System.currentTimeMillis() - startTime) / 1000;
        time.add(endTime);
        startTime = System.currentTimeMillis();

        processArousalLevel();
    }

    /**
     * Processes the measured values to get the current arousal level.
     * Sets the global arousal value to the new arousal value.
     */
    private void processArousalLevel() {

        if (timeIterator.hasNext()) {
            oldTime = newTime;
            newTime = timeIterator.next();
        }

        final float pitchRange = Math.round(((maxPitch - minPitch) * 100f) / 100f);
        final double volumeRange = Math.round(((maxVol - minVol) * 100d) / 100d);
        final long duration = endTime;

        if (pitchRange > 100.00) {
            this.arousal += 0.15;
        } else if (pitchRange < 25) {
            this.arousal -= 0.15;
        }

        if (volumeRange > 10.00) {
            this.arousal += 0.15;
        } else if (volumeRange < 3.0) {
            this.arousal -= 0.15;
        }

        if (newPitch > oldPitch) {
            this.arousal += 0.05;
        } else {
            this.arousal -= 0.05;
        }

        if (newPitch > 150.00) {
            this.arousal += 0.15;
        } else if (newPitch < 85) {
            this.arousal -= 0.15;
        }

        if (newVolume > oldVolume) {
            this.arousal += 0.05;
        } else {
            this.arousal -= 0.05;
        }

        if (newVolume > 40.0) {
            this.arousal += 0.15;
        } else if (newVolume > 35.0) {
            this.arousal += 0.05;
        } else if (newVolume < 20.0) {
            this.arousal -= 0.15;
        } else if (newVolume < 25.0){
            this.arousal -= 0.05;
        }

        if (duration <= 3) {
            this.arousal += 0.15;
        } else if (duration > 15){
            this.arousal -= 0.15;
        }

        if (duration > 30) {
            this.arousal = 0;
        }

        if (this.arousal > 1) {
            this.arousal = 1;
        } else if (this.arousal < -1) {
            this.arousal = -1;
        }

        setCurrentArousal(arousal);
        System.out.println("Current Arousal: " + arousal);
    }

    /**
     * Sets the Integer Values [-1, 0, 1] in the Speech Object for the current arousal.
     *
     * @param arousal The arousal value between -1.0 and 1.0
     */
    protected void setCurrentArousal(float arousal) {
        if (arousal > 0.5) {
            this.speech.setSoundParameters(1, 0, System.currentTimeMillis());
        } else if (arousal < -0.5) {
            this.speech.setSoundParameters(-1, 0, System.currentTimeMillis());
        } else {
            this.speech.setSoundParameters(0, 0, System.currentTimeMillis());
        }
    }

    public Camera2VideoFragment getCameraFragment(){
        return this;
    }

    public float getCurrentArousal(){
        return this.arousal;
    }
}
