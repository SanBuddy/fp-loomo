package com.example.android.camera2video;

public interface Emotion {
        public int getEmoji();
        public double getEmojiRatio();
}
