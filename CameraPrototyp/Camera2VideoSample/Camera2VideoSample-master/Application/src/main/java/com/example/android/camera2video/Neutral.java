package com.example.android.camera2video;

public class Neutral implements Emotion {
    //Image emoji;
    int emoji = R.drawable.neutral;

    double emojiRatio = 1.09;

    @Override
    public int getEmoji() {
        return emoji;
    }

    public double getEmojiRatio() {
        return emojiRatio;
    }
}
