package com.example.android.camera2video;

public class SpeechEmotion {

    int arousal;
    int valence;
    long timestamp;

    public SpeechEmotion(int arousal, int valence, long timestamp){
        this.arousal = arousal;
        this.valence = valence;
        this.timestamp = timestamp;
    }

    public int getArousal(){
        return this.arousal;
    }

    public int getValence(){
        return this.valence;
    }

    public long getTimestamp(){
        return this.timestamp;
    }
}
