package com.example.android.camera2video;

import java.util.ArrayList;

public class PictureEmotion {

    ArrayList<ArrayList<Float>> confidenceList;
    ArrayList<Float> averageConfidence;
    ArrayList<ArrayList<Integer>> lastEmotions;
    ArrayList<Integer> pictureAtmosphere;
    String atmosphereToBeSet = "neutral";
    long timestamp;

    public PictureEmotion() {

        this.confidenceList = new ArrayList<>();
        this.averageConfidence = new ArrayList<>();
        this.lastEmotions = new ArrayList<>();
        this.pictureAtmosphere = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            this.confidenceList.add(new ArrayList<Float>());
            this.averageConfidence.add((float) 0);
            this.pictureAtmosphere.add(0);
        }

    }

    public ArrayList<Float> getAverageConfidence() {
        return averageConfidence;
    }

    public ArrayList<ArrayList<Float>> getConfidenceList() {
        return confidenceList;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setAverageConfidence(ArrayList<Float> averageConfidence) {
        this.averageConfidence = averageConfidence;
    }

    public void setConfidenceList(ArrayList<ArrayList<Float>> confidenceList) {
        this.confidenceList = confidenceList;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
