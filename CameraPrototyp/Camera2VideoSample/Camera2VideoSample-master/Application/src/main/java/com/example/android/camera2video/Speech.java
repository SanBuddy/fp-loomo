package com.example.android.camera2video;

import java.util.HashMap;
import java.util.Map;

public class Speech {

    SpeechEmotion speechEmotion;
    Map<String, SpeechEmotion> resultMap = new HashMap<>();

    /**
     * Creates new instance of Speech
     */
    public Speech(){
        speechEmotion = new SpeechEmotion(0,0,System.currentTimeMillis());
        resultMap.put("oldSound", speechEmotion);
        resultMap.put("newSound", speechEmotion);
    }

    /**
     * Returns the latest recognized SpeechEmotion Object
     * @return      The latest SpeechEmotion
     */
    public SpeechEmotion emotionFromSpeech(){
        return resultMap.get("newSound");
    }

    /**
     * Returns the object before the latest recognized SpeechEmotion Object
     * @return      The predecessor of the latest SpeechEmotion
     */
    public SpeechEmotion oldEmotionFromSpeech(){
        return resultMap.get("oldSound");
    }

    /**
     * Sets the current sound parameters
     * @param arousal       Arousal of -1, 0 or 1
     * @param valence       Valence of -1, 0 or 1
     * @param timestamp     Timestamp of the recognized sound
     */
    public void setSoundParameters(int arousal, int valence, long timestamp){
        resultMap.put("oldSound", resultMap.get("newSound"));
        SpeechEmotion newSoundEmotion = new SpeechEmotion(arousal, valence, timestamp);
        this.resultMap.put("newSound", newSoundEmotion);
    }
}
