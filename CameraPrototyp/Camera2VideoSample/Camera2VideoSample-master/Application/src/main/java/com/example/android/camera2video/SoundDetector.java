package com.example.android.camera2video;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.SilenceDetector;

public class SoundDetector implements AudioProcessor {

    double threshold;
    double currentVolume;
    SilenceDetector silenceDetector;
    double pressure = 0.00002;


    public SoundDetector() {
        this.threshold = SilenceDetector.DEFAULT_SILENCE_THRESHOLD;
        this.currentVolume = 0;
        this.silenceDetector = new SilenceDetector(threshold, false);
    }

    @Override
    public boolean process(AudioEvent audioEvent) {
        handleSound();
        return true;
    }

    private void handleSound(){
        if(silenceDetector.currentSPL() > threshold){

            double p = (silenceDetector.currentSPL()/20);
            p =  Math.pow(10,p);
            p = Math.abs((p/pressure));
            double volume = (20 * Math.log10(p));
            setCurrentVolume(volume);
        }
    }
    @Override
    public void processingFinished() {
    }

    private void setCurrentVolume(double volume){
        this.currentVolume = volume;
    }

    public double getCurrentVolume(){
        return this.currentVolume;
    }

    public SilenceDetector getSilenceDetector() {
        return silenceDetector;
    }
}